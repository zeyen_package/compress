<?php

namespace Compress;
/**
 * 三种形式的图片压缩
 * @author Zeyen
 * @date 2021-05-22
 * Class Compress
 * @package Compress
 */
class Compress
{
    // 网络图片api接口
    public $request_url_for_network ="https://tpapi.faxingwu.com/image_url";
    //文件流形式api接口
    public $request_url_for_file ="https://tpapi.faxingwu.com/file_upload";
    //base64字符串格式api接口
    public $request_url_for_base64 ="https://tpapi.faxingwu.com/base64_upload";


    /**
     * 网络图片形式压缩
     * @param $image_url
     * @param int $quality_num
     * @param string $proname
     * @return bool|string
     */
    public function compressForNetwork($image_url,$quality_num=50,$proname='faxingwu')
    {
        return Helper::http_request($this->request_url_for_network,[
            'image_url'=>$image_url,
            'quality'=>$quality_num,
            'proname'=>$proname,
        ]);
    }

    /**
     * base64格式图片压缩
     * @param $image_base64
     * @param int $quality_num
     * @param string $proname
     * @return bool|string
     */
    public function compressForBase64($image_base64,$quality_num=50,$proname='faxingwu')
    {
        return Helper::http_request($this->request_url_for_file,[
            'image_base64'=>$image_base64,
            'quality'=>$quality_num,
            'proname'=>$proname,
        ]);
    }


    /**
     * 文件流形式图片压缩
     * @param $file
     * @param int $quality_num
     * @param string $proname
     * @return bool|string
     */
    public function compressForFile($file,$quality_num=50,$proname='faxingwu')
    {
        return Helper::http_request($this->request_url_for_base64,[
            'file'=>$file,
            'quality'=>$quality_num,
            'proname'=>$proname,
        ]);
    }


    





}