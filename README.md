# compress

#### 介绍
基于无损压缩图片api接口封装的压缩包

#### 软件架构
软件架构说明


#### 安装教程

1.  检测电脑中是否安装composer，如若没有，请参考composer教程安装composer
2.  在项目更目录执行 composer require jumei/compress，进行安装,或者添加在项目的composer.json文件中，再执行composer update命令


#### 使用说明

使用网络图片形式进行图片压缩

``

```
use Compress\Compress;
$compress =new Compress();
return $compress->compressForNetwork('http://www.yuming.com/202105/22/2021052214400516216656.jpg');

//返回json格式数据
```

参数说明：

​	image_url ——网络图片路径，带http

​	quality_num —— 压缩质量  100-50 整数

​	proname ——项目名称   默认faxingwu ，不可乱填写，请联系开发人员获取



响应代码：

​	code ——101 表示成功

​	msg ——提示语

​	data ——新压缩完成的返回的图片途径 （域名请联系开发人员获取，谢谢！）

#### 特别感谢

​	特别感谢合肥聚美网络科技有限公司提供的图片压缩接口  官方网站[www.faxingw.cn](www.faxingw.cn)




#### 作者信息

1.  Zeyen 邮箱 zeyen0186@aliyun.com
2.  Zeyen官方博客[www.zeyen.cn](https://www.zeyen.cn)
3.  你可以https://gitee.com/athenazetan(https://gitee.com/athenazetan) 这个地址来了解 Zeyen的其他优秀开源项目
